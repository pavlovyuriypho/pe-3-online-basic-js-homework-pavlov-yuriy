"use strict"

const slides = document.querySelectorAll('#images-wrapper .image-to-show');
let currentSlide = 0;

function carusel () {
    slides[currentSlide].classList = "image-to-show";
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].classList = "image-to-show show";
}
let go = setInterval(carusel, 3000);
function pauseCarusel () {
    const btnPause = document.querySelector('#pause');
    btnPause.addEventListener("click", function () {
        clearInterval(go);
    });
}
pauseCarusel();
function startCarusel () {
    const btnStart = document.querySelector('#start');
    btnStart.addEventListener("click", function () {
        go = setInterval(carusel, 3000);
    });
}
startCarusel();