"use strict"

const buttons = Array.from(document.getElementsByClassName('btn'));
let symbols = ['enter','s', 'e', 'o', 'n', 'l', 'z'];
let skeys =[];
let pref = 'Key';
symbols.forEach(symbol=>{
    skeys.push(pref.concat(symbol.toUpperCase()));
});
skeys[0] = 'Enter';
function toggleButton(button, buttons)
{
    buttons.forEach(btn => {
        btn.classList.remove('active')
    })
    button.classList.add('active');
}
function keyDownHandler(element)
{
    let index = skeys.indexOf(element.code);
    if (index !== -1) {
        let button = Array.from(document.getElementsByClassName(symbols[index]));
        toggleButton(button[0], buttons);
    }
}
function lightKey () {
    window.addEventListener('keydown', (element) => keyDownHandler(element));
}
lightKey();