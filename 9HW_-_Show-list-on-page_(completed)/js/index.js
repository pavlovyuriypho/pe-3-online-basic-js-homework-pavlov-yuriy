"use strict"

let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let dataOther = ["1", "2", "3", "sea", "user", 23];

function arrayList (array, parentEl) {
    let list = document.createElement('ol');
    const newArray = array.map((element) => {
        const listItem = `<li>${element}</li>`
    return listItem;
    })
    list.innerHTML = newArray.join("");
    return list;
}
document.querySelector("body").after(arrayList(data));