"use strict"

function createNewUser() {
  let newUser = {
    firstName: prompt('Напишите Ваше имя?'),
    lastName: prompt('Напишите Вашу фамилию?'),
    birthday: prompt('Напишите дату Вашего рождения dd.mm.yyyy.').split('.'),
    getAge: function () {
    let birthDate = new Date(this.birthday[2], this.birthday[1], this.birthday[0] );
    return ((Date.now() - birthDate) / (1000 * 3600 * 24 * 365))
    },
    getPassword: function () {
      return(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday[2])
    },
    getLogin: function () {
      return(this.firstName[0] + this.lastName)
    }
  }
  return(newUser);
}
let user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());