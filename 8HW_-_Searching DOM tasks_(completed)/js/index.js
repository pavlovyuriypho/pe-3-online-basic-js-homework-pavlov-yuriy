"use strict"

// 1
let changeColor = [...document.getElementsByTagName("p")];
changeColor.forEach(element => {
    element.style.background = "#ff0000";
});
// 2
let textOptionList = document.getElementById("optionsList");
console.log(textOptionList);
let optionParent = textOptionList.parentElement;
console.log(optionParent);
let optionListNodes = textOptionList.childNodes;
console.log(optionListNodes);
// 3
let paraTest = document.getElementById("testParagraph");
paraTest.textContent = "This is a paragraph";
console.log(paraTest);
//4
let headerItems = document.querySelectorAll('.main-header li');
console.log(headerItems);
headerItems.forEach(element => {
    element.classList.remove("main-nav-item");
    element.classList.add("nav-item");
})
//5
let sectionTitle = [...document.getElementsByClassName("options-list-title")];
    sectionTitle.forEach(element => {
        element.classList.remove("options-list-title");
    })
console.log(sectionTitle);