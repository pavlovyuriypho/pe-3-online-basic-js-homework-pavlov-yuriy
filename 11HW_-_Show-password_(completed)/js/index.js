"use strict"

const btnHidden = document.querySelector('.icon-password-one-hidden');
const btn = document.querySelector('.icon-password-one');
const input = document.querySelector('.form-input');
const btnConfirmHidden = document.querySelector('.icon-password-two-hidden');
const btnConfirm = document.querySelector('.icon-password-two');
const inputConfirm = document.querySelector('.pass-confirm');
const submitBtn = document.querySelector('.btn-submit');
function showPassword () {
    btnHidden.addEventListener('click', () => {
        btnHidden.classList.toggle('active');
        btn.classList.toggle('active');
            if (input.getAttribute('type') === 'password') {
                input.setAttribute('type', 'text');
            } else {
                input.setAttribute('type', 'password')
            }
    });
}
showPassword();
function passwordConfirm () {
    btnConfirmHidden.addEventListener('click', () => {
        btnConfirmHidden.classList.toggle('active');
        btnConfirm.classList.toggle('active');
        if (inputConfirm.getAttribute('type') === 'password') {
            inputConfirm.setAttribute('type', 'text');
        } else {
            inputConfirm.setAttribute('type', 'password')
        }
    });
}
passwordConfirm();

function passSubmit () {
    submitBtn.addEventListener('click', () => {
        if (input.value !== inputConfirm.value || input.value === "")  {
            submitBtn.insertAdjacentHTML('beforebegin', '<div style="color: red; margin: 0 0 17px 0;">Нужно ввести одинаковые значения!</div>');
        } else {
            alert('You are welcome!');
        }
    });
}
passSubmit();