"use strict"

let num1 = +prompt('Введите первое число.');
let num2 = +prompt('Введите второе число.');
let userOperation = prompt("Укажите какое действие необхоимо совершить: `+`, `-`, `*`, `/`?");

function calc () {
  if (userOperation === "-") {
    console.log(num1 - num2);
  }
  if (userOperation === "+") {
    console.log(num1 + num2);
  }
  if (userOperation === "*") {
    console.log(num1 * num2);
  }
  if (userOperation === "/") {
    console.log(num1 / num2);
  }
}
calc()