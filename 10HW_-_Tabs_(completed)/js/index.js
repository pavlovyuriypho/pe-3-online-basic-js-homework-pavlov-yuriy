"use strict"

const itemTabs = document.querySelectorAll(".tabs-item"); 
const tabsButton = document.querySelectorAll(".tabs-title");

tabsButton.forEach(element => {
    element.addEventListener("click", function () {
        let activeButton = element;
        let tabData = activeButton.getAttribute("data");
        let selectedTab = document.querySelector(tabData);
        tabsButton.forEach(element => {
            element.classList.remove('active');
        });
        itemTabs.forEach(element => {
            element.classList.remove('active');
        })
        activeButton.classList.add('active');
        selectedTab.classList.add('active');
    })
});