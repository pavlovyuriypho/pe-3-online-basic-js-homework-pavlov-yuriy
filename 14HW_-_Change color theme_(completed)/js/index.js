"use strict"

const changeTheme = document.querySelector(".btn");
const bodyPage = document.querySelector(".page");
let theme = localStorage.getItem('theme');


function toggleTheme() {
  if (bodyPage.classList.contains('stars')) {
    bodyPage.classList.remove('stars');
    theme = "";
  } else {
    bodyPage.classList.add('stars');
    theme = 'stars';
  }

  localStorage.setItem('theme', theme);
}

changeTheme.addEventListener('click', toggleTheme);