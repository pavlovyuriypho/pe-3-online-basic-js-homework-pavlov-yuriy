"use strict"

const data = ['hello', 'world', 23, '23', null,];

function filterBy (array, type) {
  return array.filter((item) => {
    return typeof item !== type;
  })
}
console.log(filterBy(data, 'string'));